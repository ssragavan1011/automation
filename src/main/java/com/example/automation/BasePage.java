package com.example.automation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasePage {

	public static WebDriver driver;
	public String browser="chrome";
	
		public BasePage(){
			if(driver==null) {
				if(browser!=null) {
					System.setProperty("webdriver.chrome.driver", "C:\\Users\\LENOVO\\Downloads\\chromedriver_win32\\chromedriver.exe");
					driver= new ChromeDriver();
				}
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			driver.get("http://automationpractice.com/index.php");
		}
		
		/*public void setText(WebElement element, String name) {
				element.sendKeys(name);
		}
		
		public void clickButton(WebElement element) {
				element.click();
		}*/
		
		public String getPageTitle() {
			String title =driver.getTitle();
			return title;
		}
		
		public void closeBrowser() {
			driver.close();
		}

		public void clickButton(WebElement womenTag) {
			// TODO Auto-generated method stub
			
		}
}
