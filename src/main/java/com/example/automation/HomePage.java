package com.example.automation;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage extends BasePage{

	public HomePage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
	private WebElement womenTag;
	
	public WebElement getWomenTag() {
	return womenTag;


}
}
