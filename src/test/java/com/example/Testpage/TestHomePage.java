package com.example.Testpage;

import org.junit.Assert;
import org.junit.Test;

import com.example.automation.BasePage;
import com.example.automation.HomePage;

public class TestHomePage {
	
	BasePage bp;
	HomePage hp;
	
	public TestHomePage(){
		bp= new BasePage();
		hp= new HomePage();
	}







	@Test
	public void verifyWomenPage() {
		bp.clickButton(hp.getWomenTag());
		String title=	bp.getPageTitle();
		Assert.assertEquals("Women - My Store",title);
	}
}
